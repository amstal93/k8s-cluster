# Deploy Cluster
* [References](#references)

## References
* https://www.digitalocean.com/docs/kubernetes/quickstart/
* https://www.digitalocean.com/community/tutorials/how-to-install-and-use-linkerd-with-kubernetes
* https://linkerd.io/2/getting-started/
* https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nginx-ingress-with-cert-manager-on-digitalocean-kubernetes

## Digital Ocean

### Create Cluster
```shell script
cd terraform/digitalocean/vpc
terraform apply

doctl vpcs list
```
```shell script
cd terraform/digitalocean/cluster
terraform apply

doctl kubernetes cluster list
doctl kubernetes cluster kubeconfig save do-k8s-cluster-01
```

# Install Linkerd on Cluster
```shell script
kubectl label namespace kube-system config.linkerd.io/admission-webhooks=disabled
linkerd install | kubectl apply -f -
linkerd check
linkerd dashboard > /dev/null 2>&1 &
```

## High Availability
For production usage, enable HA on install for reliable fail-over across the mesh. 
**Will require increased node size (> 2 cores/node)**

https://linkerd.io/2/features/ha/

```shell script
linkerd install --ha | kubectl apply -f -
```

# Install Dummy Services for Testing
```shell script
cd $(git rev-parse --show-toplevel)
linkerd inject k8s/test/echo1.yml | kubectl apply -f -
linkerd inject k8s/test/echo2.yml | kubectl apply -f -
```

# Ingress

## Deploy Mandatory Manifest
```shell script
mkdir -p k8s/ingress/
curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.26.1/deploy/static/mandatory.yaml > k8s/ingress/mandatory.yml
kubectl apply -f k8s/ingress/mandatory.yml
```

### Verify Ingress Pod Running
```shell script
kubectl get pods --all-namespaces -l app.kubernetes.io/name=ingress-nginx
```

## Deploy Cloud Load Balancer Ingress
```shell script
curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.26.1/deploy/static/provider/cloud-generic.yaml > k8s/ingress/cloud-generic.yml
kubectl apply -f k8s/ingress/cloud-generic.yml
```

# Bare Metal Ingress (NodePort)
Would replace `cloud-generic.yml` utilized earlier.  
Not tested yet.
```shell script
curl https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-0.32.0/deploy/static/provider/baremetal/deploy.yml  > k8s/ingress/deploy-baremetal.yml
kubectl apply -f k8s/ingress/deploy-baremetal.yml
```

## Verify Load Balancer
```shell script
kubectl get svc --namespace=ingress-nginx
CLUSTER_LB=$(kubectl get svc --namespace=ingress-nginx --no-headers=true -o=custom-columns=EXTERNAL-IP:.status.loadBalancer.ingress[0].ip)
```

# Deploy Test Ingress
```shell script
kubectl apply -f k8s/test/echo_ingress.yml
```

## Verify Test Ingress
```shell script
curl -H "Host: example.com" http://${CLUSTER_LB}       # Expect 404 from openresty
curl -H "Host: echo1.example.com" http://${CLUSTER_LB} # Expect echo1
curl -H "Host: echo2.example.com" http://${CLUSTER_LB} # Expect echo2
```

# Delete Test Ingress
```shell script
kubectl delete -f k8s/test/echo_ingress.yml
```

# Deploy Cert-Manager
```shell script
curl -L https://github.com/jetstack/cert-manager/releases/download/v0.15.0/cert-manager.yaml > k8s/cert-manager/cert-manager.yml
kubectl apply --validate=false -f k8s/cert-manager/cert-manager.yml
```

### Verify Cert-Manager Pods Running
```shell script
kubectl get pods --namespace cert-manager
```

### Deploy Staging and Production Certificate Issuers
```shell script
kubectl apply -f k8s/cert-manager/staging_issuer.yml
kubectl apply -f k8s/cert-manager/prod_issuer.yml
```

# Staging

## Create Namespace
```shell script
linkerd inject k8s/namespaces/staging/namespace-exampledomain.yml | kubectl apply -f -
```

## Install Dummy Services for Testing
```shell script
linkerd inject k8s/test/echo1.yml | kubectl -n exampledomain-staging apply -f -
linkerd inject k8s/test/echo2.yml | kubectl -n exampledomain-staging apply -f -
```

## Deploy Echo Ingress with a Self Signed Certificate

```shell script
kubectl -n exampledomain-staging apply -f k8s/namespaces/staging/echo_ingress-letsencrypt.yml
```

### Verify Staging Issuer Running
```shell script
kubectl -n exampledomain-staging describe ingress
kubectl -n exampledomain-staging describe certificate
```

### Verify Cert Manager Issued Self Signed Certificate
```shell script
wget --save-headers -O- https://echo1.k8s.staging.exampledomain.test
```

# Production

## Create Namespace
```shell script
linkerd inject k8s/namespaces/prod/namespace-exampledomain.yml | kubectl apply -f -
```

## Install Dummy Services for Testing
```shell script
linkerd inject k8s/test/echo1.yml | kubectl -n exampledomain-prod apply -f -
linkerd inject k8s/test/echo2.yml | kubectl -n exampledomain-prod apply -f -
```

## Deploy Echo Ingress with a Valid Certificate
```shell script
kubectl -n exampledomain-prod apply -f k8s/namespaces/prod/echo_ingress-letsencrypt.yml
```

### Display Certificate Info
```shell script
kubectl -n exampledomain-prod describe ingress echo-ingress
kubectl -n exampledomain-prod describe certificate echo-tls-prod
kubectl -n exampledomain-prod describe certificaterequest echo-tls-prod
kubectl -n exampledomain-prod describe order echo-tls-prod
kubectl -n exampledomain-prod describe challenge echo-tls-prod
```

### Verify Certificate
```shell script
wget --save-headers -O- https://echo1.k8s.prod.exampledomain.test
wget --save-headers -O- https://echo2.k8s.prod.exampledomain.test
```

# Add DigitalOcean container registry
https://www.digitalocean.com/docs/images/container-registry/
```shell script
doctl registry kubernetes-manifest | kubectl apply -f -
kubectl patch serviceaccount default -p '{"imagePullSecrets": [{"name": "registry-exampledomain"}]}'
```

# Deploy your services to the mesh
```shell script
linkerd inject your-k8s-manifest.yml | kubectl apply -f -
```
