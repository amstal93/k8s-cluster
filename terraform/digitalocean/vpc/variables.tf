variable "do_token" {}
variable "do_region" {
  default = "sfo2"
}
variable "do_vpc_name" {
  default = "k8s-cluster-testing"
}